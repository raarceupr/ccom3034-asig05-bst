#include "IntBinaryTree.h"
#include <iostream>

using namespace std;

int main() {
    IntBinaryTree T;

    T.insertNode(6);

    T.insertNode(3);
    T.insertNode(1);
    T.insertNode(4);

    T.insertNode(10);
    T.insertNode(20);

    T.displayInOrder();

    T.outputGraphviz();
    return 0;
}
